package iamiagkov;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static iamiagkov.Constants.*;

public class ImageEditor {

    private final BufferedImage myImage;
    private final Graphics2D g;

    public ImageEditor(String imagePath) throws IOException {
        myImage = ImageIO.read(new File(imagePath));
        g = (Graphics2D) myImage.getGraphics();
        g.setStroke(new BasicStroke(10));
        g.setColor(Color.YELLOW);
    }

    public void addYellowHighlight(int[] bounds){
        g.drawRect(bounds[0], bounds[1], bounds[2] - bounds[0], bounds[3] - bounds[1]);
    }

    public void saveImage(String name) throws IOException {
        File outputFile = new File(workingDirectory + OUTPUT_DIR + name + OUTPUT_EXT);
        ImageIO.write(myImage, "png", outputFile);
    }
}
