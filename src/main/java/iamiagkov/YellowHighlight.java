package iamiagkov;

import java.io.File;
import java.io.IOException;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static iamiagkov.Constants.*;


public class YellowHighlight {

    private static String selectedDirectory;
    public static void main(String[] args) throws IOException, ParserConfigurationException{
        File folder;
        if (args.length == 0){
            selectDirectoryUI();
            System.out.println(selectedDirectory);
            folder = new File(selectedDirectory + '\\');
            setWorkingDirectory(selectedDirectory + '\\');
        }
        else {
            if (args.length != 1) {
                throw new IOException(UNMATCH_ARGS);
            }

            folder = new File(args[0]);
            if (!folder.isDirectory()) {
                throw new IOException(NOT_DIRECTORY);
            }
            setWorkingDirectory(args[0]);
        }

        File[] allFiles = folder.listFiles();
        File outputDir = new File(workingDirectory + OUTPUT_DIR);

        for(File f: allFiles){
            if (f.isFile() && f.getName().endsWith(XML_EXT)){
                if (!outputDir.exists()){
                    outputDir.mkdir();
                }
                System.out.println(f.getName().substring(0, f.getName().lastIndexOf('.')));
                runner(f.getName().substring(0, f.getName().lastIndexOf('.')));
            }
        }

    }

    private static void selectDirectoryUI(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }

        File homeDirectory = new File (System.getProperty("user.home") + System.getProperty("file.separator"));

        JFileChooser j = new JFileChooser(homeDirectory);
        j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        j.setDialogTitle(INPUT);
        int status = j.showOpenDialog(null);
        if (status == JFileChooser.APPROVE_OPTION) {
            File file = j.getSelectedFile();
            if (file == null) {
                return;
            }
            if (file.isDirectory()) {
                selectedDirectory = j.getSelectedFile().getAbsolutePath();
            }
        }
        else{
            System.exit(0);
        }
    }

    private static void runner(String component) throws IOException, ParserConfigurationException{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder loader = factory.newDocumentBuilder();

        XMLTreeWalker walker = new XMLTreeWalker(component);
        walker.createTree(component, loader);
    }

}
