package iamiagkov;

public class Constants {

    public static final String UNMATCH_ARGS = "Too many/ too few args. Specify single directory.";
    public static final String NOT_DIRECTORY = "Designated file path is not a directory. Specify a directory";
    public static final String XML_EXT = ".xml";
    public static final String PNG_EXT = ".png";
    public static final String BOUNDS = "bounds";
    public static final String OUTPUT_DIR = "highlightOutput/";
    public static final String OUTPUT_EXT = "-output.png";

    public static final String INPUT = "SELECT INPUT DIRECTORY";


    public static String workingDirectory;

    public static void setWorkingDirectory(String directory){
        workingDirectory = directory;
    }
}
