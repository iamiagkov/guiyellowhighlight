package iamiagkov;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.TreeWalker;


import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;

import static iamiagkov.Constants.*;

public class XMLTreeWalker {
    private final ImageEditor imageEditor;

    private final String imageName;

    private int[] boxBounds = new int[4];
    public XMLTreeWalker(String imagePath) throws IOException{
        imageEditor = new ImageEditor(workingDirectory + imagePath + PNG_EXT);
        imageName = imagePath;
    }

    public void createTree(String arg, DocumentBuilder loader) {
        try {
            Document document = loader.parse(workingDirectory + arg + XML_EXT);
            DocumentTraversal traversal = (DocumentTraversal) document;
            TreeWalker walker = traversal.createTreeWalker(
                    document.getDocumentElement(),
                    NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_TEXT, null, true);

            this.traverseLevel(walker);
            imageEditor.saveImage(imageName);
        } catch (Exception e){
            String message = arg + "\n" + e;
            JOptionPane.showMessageDialog(null, message, "PARSE ERROR", JOptionPane.WARNING_MESSAGE);
            e.printStackTrace();
        }
    }

    private void traverseLevel(TreeWalker walker) {

        Node node = walker.getCurrentNode();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getFirstChild() == null){
                String line = node.getAttributes().getNamedItem(BOUNDS).toString();
                String bounds = line.substring(line.indexOf('"') + 1, line.lastIndexOf('"'));
                this.addToBoxBounds(bounds);
            }
        }

        for (Node n = walker.firstChild(); n != null; n = walker.nextSibling()) {
            traverseLevel(walker);
        }
        walker.setCurrentNode(node);
    }

    private void addToBoxBounds(String bounds){
        boxBounds[0] = Integer.parseInt(bounds.substring(bounds.indexOf('[') + 1, bounds.indexOf(',')));
        boxBounds[1] = Integer.parseInt(bounds.substring(bounds.indexOf(',') + 1, bounds.indexOf(']')));
        boxBounds[2] = Integer.parseInt(bounds.substring(bounds.lastIndexOf('[') + 1, bounds.lastIndexOf(',')));
        boxBounds[3] = Integer.parseInt(bounds.substring(bounds.lastIndexOf(',') + 1, bounds.lastIndexOf(']')));
        imageEditor.addYellowHighlight(boxBounds);
    }
}
