# GUIYellowHighlight



## Getting started

2 options to run this code:
- [ ] Download and run JAR executable
- [ ] Download compressed source code directly and use preferred IDE (Eclipse, Intellij)

Either option will result in same functionality. 

[Latest Release](https://code.wm.edu/iamiagkov/guiyellowhighlight/-/releases/Release-1.1.0)

## Running the code
### Option 1: JAR
Download the JAR, run the executable. File opener will ask for you to select a directory in which png/xml combos are stored. An output directory will be created in the same directory you selected. All output .png will be stored in there.
### Option 2: IDE (Eclipse, Intellij)
Download the source code and open as IDE project. In Intellij, this can be done by `File->New->Project from Existing Sources`. 

Main run method is located in `YellowHighlight.java`. Find the drop down menu along the top ribbon (right hand side), choose `Yellow Highlight` (or `HighlightTool` which is just JAR run in IDE), and hit green play button. By default, no run time arguments are needed and the first thing that should happen is the directory selector will appear. Output will be stored similarly as the JAR.

Alternatively, the user can provide runtime arguments in the run configuration. Only one argument is needed, and that is the absolute path to the directory in which the png/xml pairs are stored. The file select screen should not appear if run time args are provided.

## Design 
1. File opener/ Run time args step. Determine which directory will be used to run the main code. Once that is selected, designate the output directory to be a subdirectory in the originally selected directory. The main workflow will run if the `.xml` component exists so the files are not doubled up. The JFileChooser object and simple file manipulation in Java was used in this section. Having a file chooser makes it easier for a beginner to work with this tool. A new XMLTreeWalker object will be created for each pair in the directory.
2. The `XMLTreeWalker` class utilizes the base JAVA document object manager TreeWalker library to walk through the xml UI hierarchy. The XML file is loaded in as a Document object and then a TreeWalker is initialized on top of that, which will then "walk" through the XML hierarchy as if it were a tree based on the levels created by the nesting of nodes. The different nodes that are created within this tree structure conveniently have `firstChild` and `nextSibling` methods, which make the traversal a lot easier. Whenever a leaf is identified (based on the fact that is has no first child), the bounds are extracted and used in the `ImageEditor` class. An array of size 4 is used throughout store the bounds from the node. This is better than an ArrayList since there will always only be 4 different coordinates to store and does not need the extra space for a variable size array. This bounds array is then used as the main argument for the `ImageEditor` as it lines up almost one-to-one with the graphics `drawRectangle` method.
3. An `ImageEditor` is created for each xml/png pair. It utilizes the default `BufferedImage` and `Graphics` libraries in Java. The specified image is loaded in by the editor and its graphics are extracted. Whenever a leaf node is encountered, the `addYellowHighlight` method is called: it draws a yellow rectangle on the image based on the bounds provided. The only translation step that is needed from the hiearchy bounds is to subtract the second pair of (x,y) coordinates from the first as the graphics method used (x1, y1, width, height) to draw. Once the traversal is complete, the image is saved in the newly created `highlightOutput` subdirectory. This design process makes the most sense as it does not require any tree manipulation and takes advantage of the fact that a visual representation of the layout already exists. 
4. In the case that the xml file is not properly specified, this code does not attempt to fix it. However, it will throw a parse exception and will alert the user in a popup about the error. The user can use the information provided in this error message to fix the broken xml hierarchy. A useful addition to this tool would be to automatically find and fix broken uiautomator dump files. 
5. Once all the pairs are run through, all output .png's with highlighted yellow boxes for leaf components will exist in the `highlightOutput` subdirectory in the already specifed directory.

## Libraries
Maven is used for dependency management. All libraries used are available in the default JDK. Java 17 was used for this tool.
